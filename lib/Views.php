<?php
namespace lib;

class Views
{
	protected static $form_sessions;	

	function __construct()
	{
		self::$form_sessions = array();
		$this->requests = $_REQUEST;
		
	}	
	public static function render( $view, $data = null )
	{				
		$ajaxurl =  PROJECT_ROOT_URI.'?p=ajax';	
		self::$form_sessions = $_SESSION;
		if( !is_null( $data ) )	{			
			if( is_array( $data ) ){
				extract( $data );
			}
		}		
		if( !isset( $p ) ) {
			$p = null;
		}			
		
		if( isset( self::$form_sessions['customer'] ) ){

			extract( self::$form_sessions['customer'] );
		}

		if( isset( self::$form_sessions['customer_address'] ) ) {
			extract( self::$form_sessions['customer_address'] );
		}

		if( isset( self::$form_sessions['customer_payment'] ) ){
			extract( self::$form_sessions['customer_payment'] );	
		}
		
		require_once( __DIR__ .'/../views/'.$view.'.php' );
	}
}