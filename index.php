<?php 
if ( session_status() == PHP_SESSION_NONE ) {
    session_start();
}
if( $_SERVER['SERVER_NAME'] == 'localhost' ){
	/* 
	* change this value when test proeject is on localhost
	* define( "PROJECT_ROOT_URI", $_SERVER['HTTPS'].'YOUR/LOCAL/PROJECT/ENVIRONMENT/' );
	*/	
	define( "PROJECT_ROOT_URI", $_SERVER['SERVER_NAME'].'/PROJECTS/TEST/WUNDER-TEST/wunder-test-nmc/' );
}else{

	define( "PROJECT_ROOT_URI", $_SERVER['HTTPS'] );
}

require __DIR__ . '/vendor/autoload.php';
// boot database
require __DIR__ . '/config/database.php';
//use lib\Views as View;
use presenters\SitePresenter as Site;
//load registration form
//View::render('main');
Site::index();