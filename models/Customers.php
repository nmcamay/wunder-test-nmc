<?php
namespace models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Customers extends Eloquent
{
	protected  $fillable = [
        'wunder_customer_firstname',
        'wunder_customer_lastname',
        'wunder_customer_phone',
    ];

	function __construct()
	{
		# code...
	}

	protected function saveCustomer( $cust_data = null )
	{
		if( !is_null( $cust_data ) ){			
			
			$this->fill( $cust_data );
			$this->save();
			return $this->id;
		}
	}

	protected function updateCustomer( $customer_id, $cust_data )
	{
		$customer = $this->find( $customer_id );

		if( $customer ){
			$customer->wunder_customer_firstname = $cust_data['wunder_customer_firstname'];
			$customer->wunder_customer_lastname = $cust_data['wunder_customer_lastname'];
			$customer->wunder_customer_phone = $cust_data['wunder_customer_phone'];
			$customer->save();
		}else{
			return $saveCustomer( $cust_data );
		}
			
	}
}