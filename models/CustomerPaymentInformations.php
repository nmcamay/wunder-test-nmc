<?php
namespace models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomerPaymentInformations extends Eloquent
{
	protected  $fillable = [
        'wunder_customer_owner',
        'wunder_customer_iban',
        'wunder_customer_payment_id',
        'wunder_customer_id'
    ];
    private $returndata;

	function __construct()
	{
		$this->returndata = null;
	}

	protected function saveCustomerPaymentInformation( $cust_data = null )
	{
		if( !is_null( $cust_data ) ){
			$process_payment = $this->processPayment( $cust_data );
			if( is_null( $process_payment ) ){

				$this->returndata = array( 'error' => 1, 'message' => 'Error in processing payment', 'paymentDataId' => null );	
			}else{

				$cust_data['wunder_customer_payment_id'] = $process_payment;
				$this->fill( $cust_data );
				$this->save();
				$this->returndata = array( 'error' => 0, 
										'message' => 'Customer payment information saved', 
										'paymentDataId' => $process_payment );	
			}
			
			return json_decode( json_encode( $this->returndata ) );
		}
	}

	protected function processPayment( $payment_data = null )
	{		
		$push_data_arr = array( 'customerId' => $payment_data['wunder_customer_id'], 
								'iban' => $payment_data['wunder_customer_iban'],
								'owner' => $payment_data['wunder_customer_owner'] );

		$post_request_str = 'customerId='.$payment_data['wunder_customer_id'].
							'&iban='.$payment_data['wunder_customer_iban'].
							'&owner'.$payment_data['wunder_customer_owner'];
		$json_data = json_encode( $push_data_arr );		
		$endpoint = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data/";
		$ch = curl_init();		
		curl_setopt( $ch, CURLOPT_URL, $endpoint );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$response_x = curl_exec($ch);		
		curl_close ($ch);

		$response_x = '{"paymentDataId":"c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95"}';
		if( $response_x ){

			$response = json_decode( $response_x );
			return $response->paymentDataId;
		}else{
			return null;
		}
	}
}