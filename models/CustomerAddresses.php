<?php
namespace models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomerAddresses extends Eloquent
{
	protected  $fillable = [
        'wunder_customer_street',
        'wunder_customer_houseno',
        'wunder_customer_city',
        'wunder_customer_zip',
        'wunder_customer_country',
        'wunder_customer_id'
    ];

	function __construct()
	{
		# code...
	}

	protected function saveCustomerAddress( $cust_data = null )
	{
		if( !is_null( $cust_data ) ){			
			$this->fill( $cust_data );
			$this->save();
			return $this->id;
		}
	}

	protected function updateCustomerAddress( $customer_id, $cust_data )
	{
		$customer = $this->find( $customer_id );

		if( $customer ){
			$customer->wunder_customer_street = $cust_data['wunder_customer_street'];
			$customer->wunder_customer_houseno = $cust_data['wunder_customer_houseno'];
			$customer->wunder_customer_city = $cust_data['wunder_customer_city'];
			$customer->wunder_customer_zip = $cust_data['wunder_customer_zip'];
			$customer->wunder_customer_country = $cust_data['wunder_customer_country'];
			$customer->wunder_customer_id = $cust_data['wunder_customer_id'];
			$customer->save();	
		}else{
			$this->saveCustomerAddress( $cust_data );
		}

		
	}
}