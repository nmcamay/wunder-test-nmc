<?php
use Illuminate\Database\Capsule\Manager as Capsule;
 
// load composer autoload
require __DIR__ .'/../vendor/autoload.php';
 
// boot database
require __DIR__ . '/../config/database.php';
 
// create table 
Capsule::schema()->create('customers', function ($table) {
    $table->increments('id');
    $table->string('wunder_customer_firstname');
    $table->string('wunder_customer_lastname');
    $table->string('wunder_customer_phone');
    $table->timestamps();
});
 
echo 'Table created successfully!';