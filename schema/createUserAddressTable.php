<?php
use Illuminate\Database\Capsule\Manager as Capsule;
 
// load composer autoload
require __DIR__ .'/../vendor/autoload.php';
 
// boot database
require __DIR__ . '/../config/database.php';
 
// create table 
Capsule::schema()->create('customer_addresses', function ($table) {
    $table->increments('id');
    $table->string('wunder_customer_street');
    $table->string('wunder_customer_houseno');
    $table->string('wunder_customer_city');
    $table->string('wunder_customer_zip');
    $table->string('wunder_customer_country');
    
    $table->integer('wunder_customer_id')->unsigned();
    $table->foreign('wunder_customer_id')->references('id')->on('customers');
    $table->timestamps();

});
 
echo 'Table created successfully!';