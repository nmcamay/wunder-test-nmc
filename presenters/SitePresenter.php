<?php
namespace presenters;

use models\Customers as Customers;
use models\CustomerAddresses as Address;
use models\CustomerPaymentInformations as Payment;
use lib\Presenters as Presenter;
use lib\Views as Views;

class SitePresenter extends Presenter
{
	private static $requests;

	function __construct()
	{

	}

	public static function index()
	{	
		self::$requests = $_REQUEST;		
		if( isset( self::$requests ) ){
			if( isset( self::$requests['p'] ) && self::$requests['p'] == 'ajax' ){
				if( isset( self::$requests['action'] ) && self::$requests['action'] == 'init_customer_info' ){
					$_SESSION['customer'] = self::$requests['reg_data'];
				}elseif( isset( self::$requests['action'] ) && self::$requests['action'] == 'init_customer_address' ){
					$_SESSION['customer_address'] = self::$requests['reg_data'];
				}elseif( isset( self::$requests['action'] ) && self::$requests['action'] == 'init_customer_payment' ){

					$_SESSION['customer_payment'] = self::$requests['reg_data'];
					$customer_data = $_SESSION['customer'];


					if( isset( $_SESSION['customer_id'] ) && !empty( $_SESSION['customer_id'] ) ){

						$customer = $_SESSION['customer_id'];
						Customers::updateCustomer( $customer, $customer_data );						
					}else{
						$customer = Customers::saveCustomer( $customer_data );
						$_SESSION['customer_id'] = $customer;
					}
					$customer_address = $_SESSION['customer_address'];
					$customer_address['wunder_customer_id'] = $customer;

					$customer_payment = $_SESSION['customer_payment'];
					$customer_payment['wunder_customer_id'] = $customer;

					if( isset( $_SESSION['customer_id'] ) && !empty( $_SESSION['customer_id'] ) ){						
						Address::updateCustomerAddress( $customer, $customer_address );					
					}else{
						$customer = Customers::saveCustomer( $customer_data );
						Address::saveCustomerAddress( $customer_address );
					}
					
					$save_payment = Payment::saveCustomerPaymentInformation( $customer_payment );
					if( $save_payment->error == 0 ){
						unset( $_SESSION['customer'] );
						unset( $_SESSION['customer_address'] );
						unset( $_SESSION['customer_payment'] );
					}
					echo json_encode( $save_payment );
					die();
				}
				
			}else{
				return Views::render( "main", self::$requests );
			}
		}
	}

	public function ajaxGetCustomer()
	{
		//print_r( $_REQUEST );
	}
}