<?php
namespace presenters;

use lib\Presenters as Presenter;
use lib\Views as Views;

use models\Users as Users;

class UserPresenter extends Presenter
{
	
	function __construct()
	{
		# code...
	}

	public function index()
	{
		return Views::render("main");
	}

	public static function register()
	{
		return Views::render('user/register');
	}
}