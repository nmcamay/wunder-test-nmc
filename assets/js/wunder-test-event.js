$("#wunder-test-register").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true,

    onStepChanging : function( event, currentIndex, newIndex ){
    	
    	console.log( currentIndex, newIndex, ajaxurl );

    	if( currentIndex == 0 && newIndex == 1 ){
    		let regfname = $("#reg-firstname").val();
    		let reglname = $("#reg-lastname").val();
    		let regphone = $("#reg-phone").val();
    		let reg_data = {'wunder_customer_firstname' : regfname, 
    						'wunder_customer_lastname' : reglname, 
    						'wunder_customer_phone' : regphone };    		
    		users.accountRegistration( 'init_customer_info', reg_data, function( response ){
    			console.log( response );
    		});
    	}else if( currentIndex == 1 && newIndex == 2 ){

    		let regstreet = $("#reg-street").val();
    		let reghouseno = $("#reg-house-number").val();
    		let regcity = $("#reg-city").val();
    		let regzip = $("#reg-zip").val();
    		let regcountry = $("#reg-country").val();

    		let reg_data = {'wunder_customer_street' : regstreet, 
    						'wunder_customer_houseno' : reghouseno, 
    						'wunder_customer_city' : regcity,
    						'wunder_customer_zip' : regzip,
    						'wunder_customer_country' : regcountry };    		
    		users.accountRegistration( 'init_customer_address', reg_data, function( response ){
    			console.log( response );
    		});

    	}else if( currentIndex == 2 && newIndex == 3 ){
    		
    		let reg_account_owner = $("#reg-account-owner").val();
    		let reg_iban = $("#reg-iban").val();

    		let reg_data = {'wunder_customer_owner' : reg_account_owner,
    						'wunder_customer_iban' : reg_iban
    						};
    		$("#registration-alert").html( '<span class="alert alert-warning" style="color:#000000;">Processing payment...</span>' );

    		users.accountRegistration( 'init_customer_payment', reg_data, function( response ){
    			console.log( response );
    			let alert_html = '';    			
    			if( response.error == 0 ){
    				alert_html = '<span class="alert alert-success">'+ response.message +'</span>';
    			}else{
    				alert_html = '<span class="alert alert-danger">'+ response.message +'</span>';
    			}    			
    			$("#registration-alert").html( alert_html );
    		});
    	}

    	return 1;
    },
    onFinishing : function( event, currentIndex ){

    	let regaccount_number = $("#reg-account-owner").val();
    	let regaccount_iban = $("#reg-iban").val();

    	let reg_data = {'status' : 'save' };    		
    	users.accountRegistration( 'init_customer_save', reg_data, function( response ){
    		console.log( response );
    	});

    	return 1;
    }
});