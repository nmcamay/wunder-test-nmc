var users = {
    
    accountRegistration : function( action, reg_data, call_back ){
        $.ajax({
            url:ajaxurl,
            method : 'POST',
            dataType : 'json',
            data:{ 'action' : action, 'reg_data' : reg_data },
            success: function(data){                
                call_back( data );
            }
        });
    }
}