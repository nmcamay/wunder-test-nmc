**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

1. Describe possible performance optimizations for your Code.
-  I use MVP code structure for this simple registration app rather than putting everything in a single class/function.
   That way, we can assign the task/logic to a speicific into a speicific task to it's resepective Classes, ( e.g saving customer personal information, customer address information etc. )

2. Which things could be done better, than you’ve done it?
- I use Eloquent for this in handling databse objects
- I use MVP in creating this simple app. I named the directory pretty straight forward ( modles, presenters and views )

** Note **
//Problem encountered
The demo API doesnt return any data. It returns an error "No Body" upon checking on Postman.
So in order for me to test the case of getting the customers paymentDataID, i just copied from the instruction just for test purpose

On your CLI please run the following :

composer dump-autoload -o

// for importing tables
php schema/createUserTable.php
php schema/createPaymentDataTable.php
php schema/createUserAddressTable.php

If you wish to change your database credentials please edit config/database.php file

Thank you so much for this opportunity
