<?php
use presenters\SitePresenter as Site;
use presenters\CustomerPresenter as Customer;?>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="uP_qd344an40INqQYGMqWPyn0TFHpnsvxKR2oSBVkzDrnaMGFlkcTwNQ7tssNUYwkIqydQv5EG2g8gHQcjv8dQ==">
    <title>Test Registration</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/wunder-test-app.min.css" rel="stylesheet">
</head>
<div class="container">
	<div class="row">
		<?php Customer::register(); ?>
	</div>
</div>

<script>
//<![CDATA[
    var ajaxurl = "<?php echo 'http://'.$ajaxurl;?>";

//]]>
</script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" src="assets/js/wunder-users.js"></script>
<script type="text/javascript" src="assets/js/jquery.steps.min.js"></script>
<script type="text/javascript" src="assets/js/wunder-test-event.js"></script>
<body>