<div id="account-register" class="register-container">    
    <form id="wunder-test-register">
        <h3>Personal Information</h3>
        <section>
            <div class="form-group">
                <label>First Name</label>
                <input type="text" name="reg_firstname" class="form-control" id="reg-firstname" 
                    placeholder="Firstname" required="required" 
                    value="<?php echo isset( $wunder_customer_firstname ) ? $wunder_customer_firstname : "";?>">        
                </div>
                <div class="form-group">
                <label>Last Name</label>
                <input type="text" name="reg_lastname" class="form-control" id="reg-lastname" placeholder="Firstname" 
                value="<?php echo isset( $wunder_customer_lastname ) ? $wunder_customer_lastname : "";?>" required="required">        
                </div>

                <div class="form-group">
                <label>Phone</label>
                <input type="text" name="reg_phone" class="form-control" id="reg-phone" placeholder="Phone Number" 
                value="<?php echo isset( $wunder_customer_phone ) ? $wunder_customer_phone : "";?>" required="required">        
            </div>
        </section>
        <h3>Address Information</h3>
        <section>
            <div class="form-group">
                <label>Street</label>
                <input type="text" name="reg_street" class="form-control" id="reg-street"
                value="<?php echo isset( $wunder_customer_street ) ? $wunder_customer_street : "";?>" placeholder="Street">        
            </div>
            <div class="form-group">
                <label>House Number</label>
                <input type="text" name="reg_house_number" class="form-control" id="reg-house-number" placeholder="House Number" 
                value="<?php echo isset( $wunder_customer_houseno ) ? $wunder_customer_houseno : "";?>">
            </div>
            <div class="form-group">
                <label>City</label>
                <input type="text" name="reg_city" class="form-control" id="reg-city" placeholder="City" 
                value="<?php echo isset( $wunder_customer_city ) ? $wunder_customer_city : "";?>">
            </div>
            <div class="form-group">
                <label>Zip</label>
                <input type="text" name="reg_zip" class="form-control" id="reg-zip" placeholder="Zip" 
                value="<?php echo isset( $wunder_customer_zip ) ? $wunder_customer_zip : "";?>">
            </div>
            <div class="form-group">
                <label>Country</label>
                <input type="text" name="reg_country" class="form-control" id="reg-country" placeholder="Country" 
                value="<?php echo isset( $wunder_customer_city ) ? $wunder_customer_city : "";?>">
            </div>
        </section>

        <h3>Payment Information</h3>
        <section>
            <div class="form-group">
                <label>Account Owner</label>
                <input type="text" name="reg_account_owner" 
                    class="form-control" id="reg-account-owner" placeholder="Account Owner" required="required" 
                    value="<?php echo isset( $wunder_customer_owner ) ? $wunder_customer_owner : "";?>">        
            </div>
            <div class="form-group">
                <label>IBAN</label>
                <input type="text" name="reg_house_number" class="form-control" id="reg-iban" placeholder="IBAN" 
                value="<?php echo isset( $wunder_customer_iban ) ? $wunder_customer_iban : "";?>">
            </div>            
        </section>
        <h3>Payment Information</h3>
        <section>
            <div class="step-alert">
                <div id="registration-alert">
                </div>
            </div>
        </section>
        <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </form>
</div>