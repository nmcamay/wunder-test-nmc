-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2018 at 12:47 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wundertest_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `wunder_customer_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `wunder_customer_firstname`, `wunder_customer_lastname`, `wunder_customer_phone`, `created_at`, `updated_at`) VALUES
(1, 'Lana', 'Lang', '9057611883', '2018-10-07 04:12:24', '2018-10-07 04:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `wunder_customer_street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_houseno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_addresses`
--

INSERT INTO `customer_addresses` (`id`, `wunder_customer_street`, `wunder_customer_houseno`, `wunder_customer_city`, `wunder_customer_zip`, `wunder_customer_country`, `wunder_customer_id`, `created_at`, `updated_at`) VALUES
(1, '955 Roselawn Ave,', '123456', 'Toronto', 'M6B 1B6', 'Toronto', 1, '2018-10-07 04:14:22', '2018-10-07 04:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payment_informations`
--

CREATE TABLE `customer_payment_informations` (
  `id` int(10) UNSIGNED NOT NULL,
  `wunder_customer_owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_iban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wunder_customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_payment_informations`
--

INSERT INTO `customer_payment_informations` (`id`, `wunder_customer_owner`, `wunder_customer_iban`, `wunder_customer_payment_id`, `wunder_customer_id`, `created_at`, `updated_at`) VALUES
(1, 'asdf asdfasdf', '2323', 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95', 1, '2018-10-07 04:27:36', '2018-10-07 04:27:36'),
(2, 'asdf asdfasdf', '2323', 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95', 1, '2018-10-07 04:27:54', '2018-10-07 04:27:54'),
(3, 'asdf asdfasdf', '2323', 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95', 1, '2018-10-07 04:29:13', '2018-10-07 04:29:13'),
(4, 'asdf asdfasdf', '2323', 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95', 1, '2018-10-07 04:30:03', '2018-10-07 04:30:03'),
(5, 'asdf asdfasdf', '2323', 'c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95', 1, '2018-10-07 04:31:09', '2018-10-07 04:31:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_addresses_wunder_customer_id_foreign` (`wunder_customer_id`);

--
-- Indexes for table `customer_payment_informations`
--
ALTER TABLE `customer_payment_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_payment_informations_wunder_customer_id_foreign` (`wunder_customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_payment_informations`
--
ALTER TABLE `customer_payment_informations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
